const express = require("express")
const cors = require("cors")
const bodyParser = require("body-parser")
const dotenv = require("dotenv")
const multer = require('multer')
const upload = multer({ dest: './uploads/' })

dotenv.config()

const app = express()

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get("/", (req, res) => {
    res.send("<h2>Welcome to the Node Hypermedia API</h2>")
})

let i = 0

app.get("/users", (req, res) => {
    res.send([{ id: 1, name: "toto" }, { id: 2, name: "titi" }, { id: 3, name: "tata" }])
})

app.get("/news", (req, res) => {
    res.send(`<h2>News ${i} </h2>`)
    i++
})

app.get("/bigbox", (req, res) => {
    res.send(`<div id="growing-box" class="grow" style="height: 300px; width: 300px; background-color: blue;">Big Box</div>`)
})

app.post("/message", async (req, res) => {
    res.send("<div><h3>Hello World</h3></div>")
})

app.post("/history", async (req, res) => {
    res.set({
        "Last-Modified": "Wednesday, 20 Sept 2023"
    })
    res.send("<div><h3>Hello World</h3></div>")
})

app.post("/htmx", async (req, res) => {
    res.send(`
        <div>
            <h3>I am loading some htmx stuff</h3>
            <button type="button" hx-get="http://localhost:5000" hx-target="#destination">Load root</button>    
        </div>
    `)
})

app.post("/script", async (req, res) => {
    res.send(`
        <div>
            <h3>I am loading a script</h3>
            <script>
                alert("coucou")
            </script>    
        </div>
    `)
})

app.post("/oob", async (req, res) => {
    res.send(`<div><h3 id="target2" hx-swap-oob="true">Hello World</h3>This goes into the main target</div>`)
})

app.post("/load-subset", async (req, res) => {
    res.send(`<div><h3 id="target2">Hello World</h3>This goes into the main target</div>`)
})

app.post("/slow_message", async (req, res) => {
    setTimeout(() => {
        res.send("<div><h3>Slow Hello World</h3></div>")
    }, 3000)
})

app.post("/echo_payload", async (req, res) => {
    const email = req.body.email
    const pass = req.body.pass
    res.send(`<div><b>Email:</b> ${email}, <b>Password:</b> ${pass}</div>`)
})

app.post("/upload", upload.single('file'), async (req, res) => {
    const filePath = req.file.path
    console.log(filePath)
    res.send(`<div><b>Upload successful</b></div>`)
})

const PORT = process.env.PORT || 5000

app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`)
})