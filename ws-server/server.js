const { WebSocketServer } = require("ws")

const server = new WebSocketServer({ port: 5005 })

server.on("connection", (socket) => {
    console.log("Client connected")
    socket.on("message", (data) => {
        const sent_data = JSON.parse(data)
        console.log(sent_data.chat_message)
        socket.send(`
            <div id="chat_box" hx-swap-oob="beforeend">
                <h3>You sent: ${sent_data.chat_message}</h3>
            </div>
        `)
    })
})